app.service('StoreMigrationBulkService',function($http) {
	this.searchMigrationDisplayInBulk=function($scope){searchMigrationDisplayInBulk($scope,$http);}
	this.confirmedToMigrationService=function($scope){confirmedToMigrationService($scope,$http);}
	this.searchAgain=function($scope){
		$scope.flagconfirmedToMigration=false;
		showMigrationDisplayList($scope,new Array());
		removeErrorMsg();
	};
	
});

function searchMigrationDisplayInBulk($scope,$http) {
	var selectedBusiness =getSelectedBusinessDivisionNumber($scope);
	var storenumber=$scope.storeNumber;
	if(validateStoreNumber(storenumber)){
		return;
	}
	if(validateDevModelNumber()){
		return;
	}
	myobject={'storeNumber' : storenumber,
				'divisionName' : selectedBusiness,
				'devCfgId' : $("#devCfgId").val() ,
				'migrateToDevCfgId':$("#migrateToDevCfgId").val(),
				};
	$("#searchresultspan").html("<br/>Waiting for result");
	$("#searchresultspan").css({"font-size":"12px","color":"black"});
	$http({
        url: url_device_search_bulk_migration,
        method: "POST",
        data:Object.toparams(myobject),
        headers: header_type
    })
    .then(function(serverresponse) {
    	var response=serverresponse.data;
    	if(response.responseStatus=="SUCCESS"){
	    	var deviceDetailData=response.responseData;
	    	var dataLength=deviceDetailData.length;
			console.log ("showResultMigrationDisplayList length:"+ dataLength);
			//Used this value for confirmation
			$scope.noofdevices=dataLength;
			if (dataLength > 0) {
				$scope.flagconfirmedToMigration=true;
				$("#searchresultspan").html("");
			} else {
				$scope.flagconfirmedToMigration=false;
				$("#searchresultspan").html("<br/>No data found");
				$("#searchresultspan").css({"font-size":"12px","color":"red"});
			}
			manageServerReponse(response);
	    	showMigrationDisplayList($scope,deviceDetailData);
    	}
    	else
    	{
    		$("#searchresultspan").html("");
    		manageServerReponse(response);
    	}
    }, 
    function(response) { // optional
    	$("#searchresultspan").html("Unable to complete request");
    	$("#searchresultspan").css({"font-size":"12px","color":"red"});
    	manageFailedServerResponse(response); 
    });
}


function confirmedToMigrationService($scope,$http) {
		var selectedBusiness =getSelectedBusinessDivisionNumber($scope);
		var storenumber=$scope.storeNumber;
		if(validateStoreNumber(storenumber)){
			return;
		}
		if(validateDevModelNumber()){
			return;
		}
		myparameters={'storeNumber' : storenumber,
				'divisionName' : selectedBusiness,
				'devCfgId' : $("#devCfgId").val() ,
				'migrateToDevCfgId':$("#migrateToDevCfgId").val(), 
				'businessReason':$scope.results
				};
		$("#confirmedToMigration").hide();
		$("#searchAgain").hide();
		$("#waitingresponsediv").html("<br/>Waiting for response from server");
		$("#waitingresponsediv").css({"font-size":"18px","color":"gray"});
		
		$http({
		    url: url_device_confirmed_migration,
		    method: "POST",
		    data:Object.toparams(myparameters),
		    headers: header_type
		})
		.then(function(serverresponse) {
		    	var response=serverresponse.data;
		    	if(response.responseStatus=="SUCCESS"){
		    		manageServerReponse(response);
		    		displayOperationResultOnResultPage($scope,$http,response.responseData);
		    	}
		    	else
		    	{
		    		$("#searchresultspan").html("");
		    		$("#waitingresponsediv").html("");
		    		$("#confirmedToMigration").show();
		    		$("#searchAgain").show();
		    		manageServerReponse(response);
		    	}
		}, 
		function(response) { // optional
			manageFailedServerResponse(response);
		});
}

function showMigrationDisplayList($scope,deviceDetailData) 
		{
				$.fn.dataTable.ext.errMode = 'none';
				$('#StoreLabelStatus').on('page.dt,error.dt', function() {
					console.log( 'An error has been reported by DataTables: ');
				}).DataTable({
					"data" : deviceDetailData,
					"fnDrawCallback" : function(oSettings) {
						/* applyColor(); */
					},
					"columns" : [ {
						"data" : "deviceBarcode",
						"sWidth" : "120px"
					}, {
						"data" : "devicStatus",
					},{
						"data" : "migrateToDeviceBarcode",
					},{
						"data" : "imageType",
					},{
						"data" : "associationStatus",
					}, {
						"data" : "associatedPartNumber",
					}, {
						"data" : "parentPartNumber",
						"visible": false,
					},  {
						"data" : "lastUpdatedDate",
						"visible": false,
					} ,{
						"data" : "rowDisplaySeq",
						"visible": false,
					} ],
					responsive : true,
					searching: true,
					"bSort" : true,
					"bDestroy" : true,
					"order": [[ 0, "asc" ]],
					"columnDefs": [ { orderable: false, targets: [1] }],
					"initComplete" : function() {
						/* applyColor(); */
					}

				});
	}

function validateDevModelNumber()
{
	var flagforcheck=false;
	var flagforcheck1=false;
	if($("#devCfgId").val()=="")
		{
			$("#devCfgId").next().html("Please select option");
			$("#devCfgId").next().css({"font-size":"12px","color":"red"});
			flagforcheck=true;
		}
	else
		{
		$("#devCfgId").next().html("");
		}
	if($("#migrateToDevCfgId").val()=="")
		{
			$("#migrateToDevCfgId").next().html("Please select option");
			$("#migrateToDevCfgId").next().css({"font-size":"12px","color":"red"});
			flagforcheck1=true;
		}
	else
		{
		$("#migrateToDevCfgId").next().html("");
		}
	return flagforcheck || flagforcheck1;
}
