app.service('ViewAllDataService',function($http) {
	this.getAllBypassResult=function($scope){getAllBypassResultFromServer($scope,$http);}
	this.searchAgain=function($scope){
		$scope.flagshowhistoryresult=false;
		$scope.showResultPage=false;
		$("#showsearchPage").removeClass("ng-hide");
		$("#showsearchPage").show();
		showHistoryDisplayList($scope,new Array());
		removeErrorMsg();
	};
});

function getAllBypassResultFromServer($scope,$http) {
	
	    showWaitingBox("searchresultspan");
		myobject={};
		
		$http({
	        url: url_get_all_data_list,
	        method: "GET",
	        data:Object.toparams(myobject),
	        headers: header_type
	    })
	    .then(function(serverresponse) {
	    	var response=serverresponse.data;
	    	if(response.statusCode=="SUCCESS"){
	    			var resultData=response.resultlist;
			    	var dataLength=resultData.length;
					if (dataLength > 0) {
						$scope.flagshowhistoryresult=true;
						$("#searchresultspan").html("");
					} else {
						$scope.flagshowhistoryresult=false;
						$("#searchresultspan").html("<br/>No data found");
						$("#searchresultspan").css({"font-size":"12px","color":"red"});
					}
					manageServerReponse(response);
					showHistoryDisplayList($scope,resultData);
	    	}
	    	else
	    	{
	    		$("#searchresultspan").html("");
	    		manageServerReponse(response);
	    	}
	    	
	    }, 
	    function(response) { // optional
	    	$("#searchresultspan").html("<br/>Unable to complete request");
	    	$("#searchresultspan").css({"font-size":"12px","color":"red"});
	    	manageFailedServerResponse(response); 
	    });
}

function showHistoryDisplayList($scope,resultData) 
{
	console.log(resultData)
		$.fn.dataTable.ext.errMode = 'none';
		$('#result_table').on('page.dt,error.dt', function() {
			console.log( 'An error has been reported by DataTables: ');
		}).DataTable({
			"data" : resultData,
			"fnDrawCallback" : function(oSettings) {
				/* applyColor(); */
			},
			"columns" : [{
				"data" :null,
				"render": function ( data, type, row ) {
                    return data.id.byp_PRD_TYP_CD;
				}
			},{
				"data" :null,
				"render": function ( data, type, row ) {
                    return data.id.byp_PRD_CD;
                }
			},{
				"data" : "lst_MTC_TS",
			}, {
				"data" : "crt_USR_ID",
			}, {
				"data" : "action",
			}],
			responsive : true,
			searching: true,
			"bSort" : true,
			"bDestroy" : true,
			"order": [],
			"initComplete" : function() {
				/* applyColor(); */
			}

		});
}
