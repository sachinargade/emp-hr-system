app.controller('ViewAllDataController',function ($scope,ViewAllDataService,promptUP) {
	   
	   $("#headertitle").html("View All Data");
	
		$scope.flagshowhistoryresult=false;
		$scope.showResultPage=false;
	    
	    $scope.getAllBypassResult=function(){
	    	ViewAllDataService.getAllBypassResult($scope);
	    };
	    $scope.searchAgain=function(){
	    	ViewAllDataService.searchAgain($scope);
	    };
	    
});

function viewActivityDetails(activityId,storeNumber)
{
	console.log("activityId: "+activityId);
	var myobject={"activityId":activityId,"storeNumber":storeNumber};
	 $.post(url_activity_by_id, myobject, function(serverresponse){
		    $("#showResultPage").show();
	        $("#showResultPage").removeClass("ng-hide");
	        $("#showsearchPage").hide();
	        $scopeglobal.activityId=activityId;
	        //$scopeglobal.storeNumber=storeNumber;
	        
	        var response=serverresponse;
	    	if(response.responseStatus=="SUCCESS"){
	    		manageServerReponse(response);
	    		displayOperationResultOnResultPage($scopeglobal,null,response.responseData);
	    	}   else   	{
	    		$("#searchresultspan").html("");
	    		manageServerReponse(response);
	    	}
	    }).fail(function(response) {
	    	manageFailedServerResponse(response);
	    });
}
function CancelView()
{
	$("#showResultPage").hide();
    $("#showResultPage").addClass("ng-hide");
    $("#showsearchPage").show();
}
