
Object.toparams = function ObjecttoParams(obj) {
    var p = [];
    for (var key in obj) {
        p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    return p.join('&');
};

function pad(n, z) {
	  z = z || '0';
	  n = n + '';
	  return n.length >= 3 ? n : new Array(3 - n.length + 1).join(z) + n;
}

function timeout(src)
{
	setTimeout(function(){
		$("#"+src).html("");
		$("#"+src).css({});
		 },10000);
}

function showWaitingBox(src)
{
	 $("#"+src).html("<br/><img src='"+contextPath+"/system-activity/images/loading.gif'></img>");
	 $("#"+src).css({"font-size" : "12px","color" : "black"});
}
function manageFailedServerResponse(response){
	  if(response.status==401){
			$("#headertitle").next().html("<br/>Your session is expired, Please log in again");
			$("#headertitle").next().css({"font-size":"12px","color":"red"});
			 var url=window.location.href;
			 url=url.replace("index","login");
			 var start = new Date().getTime();
			   var end = start;
			   while(end < start + 5000) {
			     end = new Date().getTime();
			  }
			 window.location.href= url;
			}
		else {
			$("#errorDiv").show();
			$("#errorMsg").html("Unable to complete request");
			$("#errorMsg").css({"font-size":"15px","color":"red"});
	   	 	console.log("Unable to complete request");
	   	 	$("#searchresultspan").html("");
	   	    timeout(errorMsg);
		}
}
function removeErrorMsg(){
	$("#errorDiv").hide();
	$("#errorMsg").html("");
}
function manageServerReponse(response){
	if(response.statusCode=="FAILURE"){
		$("#errorDiv").show();
		var errorarray=response.errors;
		$("#errorMsg").html("");
		var errorHtml="";
		if(errorarray!=null)
		{
		for(var i=0;i<errorarray.length;i++)
			{
			errorHtml+="<br/>"+errorarray[i];
			}
		}else{
			errorHtml="Error in request processing";
		}
		$("#errorMsg").html(errorHtml);
		$("#errorMsg").css({"font-size":"15px","color":"red"});
		 timeout("errorMsg");
	}else
		{
		   var invalidbarcode=response.errorMsg;
		   if( typeof invalidbarcode!="undefined" && invalidbarcode.indexOf("[]")==-1)
			   {
			    $("#errorDiv").show();
				$("#errorMsg").html(invalidbarcode);
				$("#errorMsg").css({"font-size":"15px","color":"red"});
			   }
		   else{
			$("#errorDiv").hide();
			$("#errorMsg").html("");
		   }
		}
}

function requiredFieldValidation()
{
	var flagValidation=false;
	$(".requiredthreedigitnumber").each(function(){
		var localflag=showRequiredThreeDigitNumberMessage(this);
		if(localflag && !flagValidation)
			flagValidation=localflag;
	});
	
	$(".requiredtwodigitnumber").each(function(){
		var localflag=showRequiredTwoDigitNumberMessage(this);
		if(localflag && !flagValidation)
			flagValidation=localflag;
	});
	
	$(".requiredfourfivedigitnumber").each(function(){
		var localflag=showRequiredFourFiveDigitNumberMessage(this);
		if(localflag && !flagValidation)
			flagValidation=localflag;
	});
	
	$(".requiredOnlyDigit").each(function(){
		var localflag=showRequiredOnlyDigitMessage(this);
		if(localflag && !flagValidation)
			flagValidation=localflag;
	});
	$(".requiredinput").each(function(){
		var localflag=showRequiredFieldMessage(this);
		if(localflag && !flagValidation)
			flagValidation=localflag;
	});
	
	return flagValidation;
}
function addEventOnfield()
{
	$(".requiredthreedigitnumber").blur(function(){
		showRequiredThreeDigitNumberMessage(this);
	});
	
	$(".requiredtwodigitnumber").blur(function(){
		showRequiredTwoDigitNumberMessage(this);
	});
	$(".requiredfourfivedigitnumber").blur(function(){
		showRequiredFourFiveDigitNumberMessage(this);
	});
	$(".requiredOnlyDigit").blur(function(){
		showRequiredOnlyDigitMessage(this);
	});
	$(".requiredinput").blur(function(){
		showRequiredFieldMessage(this);
	});
	
	$(".requiredinput").change(function(){
		showRequiredFieldMessage(this);
	});
}
function showRequiredFieldMessage(element)
{
	var flagValidation=false;
	var value=$(element).val();
	$(element).next().html("");
	if( $(element).attr("disabled")!="disabled" && (typeof value=="undefined" || value=="")){
		showError(0,element)
		flagValidation=true;
	}
	return flagValidation;
}

function showRequiredThreeDigitNumberMessage(element)
{
	var flagValidation=false;
	var value=$(element).val();
	$(element).next().html("");
	if($(element).attr("disabled")!="disabled" && (typeof value=="undefined" || value=="")){
		showError(0,element)
		flagValidation=true;
	}else if($(element).attr("disabled")!="disabled" && !$.isNumeric(value)){
		showError(1,element)
		flagValidation=true;
	}else if($(element).attr("disabled")!="disabled" && (0>value || value>1000)){
		showError(2,element)
		flagValidation=true;
	}
	return flagValidation;
}

function showRequiredTwoDigitNumberMessage(element)
{
	var flagValidation=false;
	var value=$(element).val();
	$(element).next().html("");
	if($(element).attr("disabled")!="disabled" && (typeof value=="undefined" || value=="")){
		showError(0,element)
		flagValidation=true;
	}else if($(element).attr("disabled")!="disabled" && !$.isNumeric(value)){
		showError(1,element)
		flagValidation=true;
	}else if($(element).attr("disabled")!="disabled" && (0>value || value>100)){
		showError(3,element)
		flagValidation=true;
	}
	return flagValidation;
}


function showRequiredFourFiveDigitNumberMessage(element)
{
	var flagValidation=false;
	var value=$(element).val();
	$(element).next().html("");
	if($(element).attr("disabled")!="disabled" && (typeof value=="undefined" || value=="")){
		showError(0,element)
		flagValidation=true;
	}else if($(element).attr("disabled")!="disabled" && !$.isNumeric(value)){
		showError(1,element)
		flagValidation=true;
	}else if($(element).attr("disabled")!="disabled" && (999>value || value>100000)){
		showError(4,element)
		flagValidation=true;
	}
	return flagValidation;
}

function showRequiredOnlyDigitMessage(element)
{
	var flagValidation=false;
	var value=$(element).val();
	$(element).next().html("");
	if($(element).attr("disabled")!="disabled" && (typeof value=="undefined" || value=="")){
		showError(0,element)
		flagValidation=true;
	}else if($(element).attr("disabled")!="disabled" && !$.isNumeric(value)){
		showError(1,element)
		flagValidation=true;
	}
	return flagValidation;
}

function showError(errormessageIndex,element)
{
	var ErrorArray=['This is required field',
	                'Required only digits',
	                'Input can not be more than three digit',
	                'Input can not be more than two digit',
	                'Input can be four or five digit',
	                'Input should be eight digit']
	$(element).next().html(ErrorArray[errormessageIndex]);
	$(element).next().css({"font-size":"12px","color":"red"});
	$("#errorDiv").hide();
	$("#errorMsg").html("");
}


