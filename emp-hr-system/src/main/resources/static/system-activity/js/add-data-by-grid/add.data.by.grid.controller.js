app.directive("inputDisabled", function(){
	
	  return function(scope, element, attrs){
	    scope.$watch(attrs.inputDisabled, function(val){
	      if(val)
	    	  element.attr("disabled", "disabled");
	      else
	    	  element.removeAttr("disabled");
	    });
	  }
});

app.controller('AddDataByGridController',function ($scope,$compile,AddDataByGridService,$http) {
	$("#headertitle").html("Add Data by Grid");
	
	$scope.flagshowhistoryresult=false;
	$scope.showResultPage=false;
	$scope.showEntryPage=false;
	$scope.showSubmitButton=false;
	
	$scope.bypasslevel='';
	$scope.action='';
	$scope.editableDiv=true;
	$scope.editableLin=true;
	$scope.editableSbl=true;
	$scope.editableCls=true;
	$scope.editableItm=true;
	$scope.editableSpg=true;
	
	
	$scope.entrycount=0;
	$scope.recordadded=0;
	$scope.successcount=0;
	$scope.failedcount=0
	
	$scope.map={};
	
	//Add validation event on filed
	addEventOnfield();
	
	$scope.fieldSelection=function(){
		resetFormData($scope);
		$(".requiredfield").html("");
		AddDataByGridService.filedSlection($scope);
	}
    $scope.verifySaveinGrid=function(){
    	$scope.showSubmitButton=true;
    	AddDataByGridService.verifySaveinGrid($scope,$compile);
    };

    $scope.submitGridData=function(){
    	$scope.showEntryPage=true;
    	$scope.showSubmitButton=false;
    	AddDataByGridService.submitGridData($scope);
    }
    $scope.removeRow = function(idindex){
    	$("#tr_"+idindex).remove();
        $scope.entrycount=$scope.entrycount-1;
        $scope.map['map_'+idindex]='';
    }
    $scope.openEntryGrid=function(){
    	$scope.showEntryPage=false;
    	$scope.entrycount = 0;
    	$scope.recordadded= 0;
    	$("tr[id^='tr_']").remove();
    	//$scope.map={};
    }
    
});



	