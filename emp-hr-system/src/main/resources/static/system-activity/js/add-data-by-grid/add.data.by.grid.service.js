app.service('AddDataByGridService', function($http) {
	this.verifySaveinGrid = function($scope,$compile) {
		verifySaveinGridFromServer($scope,$compile, $http);
	}
	this.filedSlection = function($scope) {
		filedSlection($scope, $http);
	}
	this.submitGridData=function($scope){
		submitGridDatatoServer($scope,$http);
	}
});

function filedSlection($scope) {
	var selval = $scope.bypasslevel;
	switch (selval) {
	case "DIV":
		$scope.editableDiv = false;
		$scope.editableLin = true;
		$scope.editableSbl = true;
		$scope.editableCls = true;
		$scope.editableItm = true;
		$scope.editableSpg = true;
		break;
	case "LIN":
		$scope.editableDiv = false;
		$scope.editableLin = false;
		$scope.editableSbl = true;
		$scope.editableCls = true;
		$scope.editableItm = true;
		$scope.editableSpg = true;
		break;
	case "SBL":
		$scope.editableDiv = false;
		$scope.editableLin = false;
		$scope.editableSbl = false;
		$scope.editableCls = true;
		$scope.editableItm = true;
		$scope.editableSpg = true;
		break;
	case "CLS":
		$scope.editableDiv = false;
		$scope.editableLin = false;
		$scope.editableSbl = false;
		$scope.editableCls = false;
		$scope.editableItm = true;
		$scope.editableSpg = true;
		break;
	case "ITM":
		$scope.editableDiv = false;
		$scope.editableLin = true;
		$scope.editableSbl = true;
		$scope.editableCls = true;
		$scope.editableItm = false;
		$scope.editableSpg = true;
		break;
	case "SPG":
		$scope.editableDiv = false;
		$scope.editableLin = true;
		$scope.editableSbl = true;
		$scope.editableCls = true;
		$scope.editableItm = true;
		$scope.editableSpg = false;
		break;
	}
}
function submitGridDatatoServer($scope,$http){
	var griddata = [];
    var index=0
	angular.forEach($scope.map, function(value, key){
		console.log(key);
		if(value!=""){
		 griddata[index++]=value;
		}
	   });
    $scope.formData=griddata;
    console.log($scope.formData)
    if(index==0){
    	$("#submitresultspan").html("<br/>No data available to submit");
    	$("#submitresultspan").css({
    		"font-size" : "12px",
    		"color" : "black"
    	});
    	return true;
    }
    
    showWaitingBox("submitresultspan");
    var response = $http.post(url_addItemList, $scope.formData);
	response.success(function(data, status, headers, config) {
		if (data.statusCode== "SUCCESS") {
			var deviceDetailData = data.resultlist;
			var dataLength = deviceDetailData.length;
			if (dataLength > 0) {
				$("#submitresultspan").html("Record submitted successfully");
				$("#submitresultspan").css({
					"font-size" : "12px",
					"color" : "green"
				});
				showItemAddedSubmitResultInGrid($scope, deviceDetailData);
				timeout("submitresultspan");
			}
		}
		else{
			$("#submitresultspan").html("");
			$("#submitresultspan").css({
				"font-size" : "12px",
				"color" : "red"
			});
			manageServerReponse(data);
			timeout("submitresultspan");
		}
	});
	response.error(function(data, status, headers, config) {
		$("#searchresultspan").html(JSON.stringify({data: data}));
		$("#searchresultspan").css({
			"font-size" : "12px",
			"color" : "red"
		});
		manageFailedServerResponse(response);
	});
	
}
function verifySaveinGridFromServer($scope, $compile,$http) {
	// var lin=$scope.lin;
	if(requiredFieldValidation())
		return true;
	
	var div = ($scope.div === undefined) ? '' : $scope.div;
	var lin = ($scope.lin === undefined) ? '' : $scope.lin;
	var sbl = ($scope.sbl === undefined) ? '' : $scope.sbl;
	var cls = ($scope.cls === undefined) ? '' : $scope.cls;
	var itm = ($scope.itm === undefined) ? '' : $scope.itm;
	var spg = ($scope.spg === undefined) ? '' : $scope.spg;
	var action = ($scope.action === undefined) ? '' : $scope.action;
	var myobject = {
		'bypassLevel' : $scope.bypasslevel,
		'div' : div,
		'lin' : lin,
		'sbl' : sbl,
		'cls' : cls,
		'itm' : itm,
		'spg' : spg,
		'action' : action
	};
	
	var parameter = Object.toparams(myobject);
	var url = url_validateItem + '?' + parameter;
	$("#searchresultspan").html("<br/>Waiting for result");
	$("#searchresultspan").css({
		"font-size" : "12px",
		"color" : "black"
	});
	$http({
		url : url,
		method : "GET",
		data : '',
		headers : header_type
	}).then(function(serverresponse) {
		var response = serverresponse.data;
		if (response.statusCode == "SUCCESS") {
			var deviceDetailData = response.resultlist;
			var dataLength = deviceDetailData.length;
			if (dataLength > 0) {
				$("#searchresultspan").html("Record validate and added in grid successfully");
				$("#searchresultspan").css({
					"font-size" : "12px",
					"color" : "green"
				});
				timeout("searchresultspan");
			} else {
				$("#searchresultspan").html("<br/>No data found");
				$("#searchresultspan").css({
					"font-size" : "12px",
					"color" : "red"
				});
				timeout("searchresultspan");
			}
			manageServerReponse(response);
			showItemAddedInGrid($scope,$compile, deviceDetailData);
		} else {
			$("#searchresultspan").html("Invalid records entered");
			$("#searchresultspan").css({
				"font-size" : "12px",
				"color" : "red"
			});
			manageServerReponse(response);
		}

	}, function(response) { // optional
		$("#searchresultspan").html("<br/>Unable to complete request");
		$("#searchresultspan").css({
			"font-size" : "12px",
			"color" : "red"
		});
		manageFailedServerResponse(response);
	});
}

function showItemAddedInGrid($scope,$compile, deviceDetailData) {
	$scope.entrycount = $scope.entrycount + 1;
	$scope.recordadded= $scope.recordadded + 1;;
	var resObj=deviceDetailData[0];
	resObj.action= $scope.action;
	$scope.map['map_'+$scope.recordadded]=resObj;
	console.log($scope.map);
	var $el= $('<tr role="row" id="tr_'+$scope.recordadded+'">'
			+'<td>' + resObj.productCode+'</td>'
			+'<td>' + resObj.product + '</td>'
			+'<td>'+ resObj.action + '</td>'
			+'<td>Pending</td>'
			+'<td><img src="'+contextPath+'/system-activity/images/remove.png" style="cursor: pointer;" '
			+'id="remove_'+$scope.recordadded+'" data-ng-click="removeRow('+$scope.recordadded+')"></img></td>'
			+'</tr>').appendTo('#recordgrid');
	
	$compile($el)($scope);
	$scope.bypasslevel='';
	resetFormData($scope);
}

function showItemAddedSubmitResultInGrid($scope, deviceDetailData) {
	//$scope.map='';
	$scope.entrycount = 0;
	$scope.recordadded= 0;
	$scope.successcount=0;
	$scope.failedcount=0
	
	$scope.map={};
	$("tr[id^='tr_']").remove();
	
	for(i=0;i<deviceDetailData.length;i++) {
		$scope.entrycount = $scope.entrycount + 1;
		$scope.recordadded= $scope.recordadded + 1;
		var resObj=deviceDetailData[i];
		var style="";
		if(resObj.status=="SUCCESS"){
			$scope.successcount = $scope.successcount + 1;
			style="color:white;background-color:green";
		}
		else{
			$scope.failedcount = $scope.failedcount + 1;
			style="color:white;background-color:red";
		}
		var $el= $('<tr role="row" id="tr_'+$scope.recordadded+'">'
				+'<td>'+ resObj.productCode+'</td>'
				+'<td>'+ resObj.product + '</td>'
				+'<td>'+ resObj.action + '</td>'
				+'<td style="'+style+'">'+ resObj.status + '</td>'
				+'<td></td>'
				+'</tr>').appendTo('#recordgrid');
	}
}

function resetFormData($scope){
	$scope.div=''; 
	$scope.lin='';
	$scope.sbl=''; 
	$scope.cls='';
	$scope.itm='';
	$scope.spg='';
	$scope.action='';
}
