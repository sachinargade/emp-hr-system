app.service('FileUploadService', function($http,$q) {
	this.uploadFile = function($scope) {
		uploadFiletoServer($scope, $q, $http);
	}
	this.submitfileGridData= function($scope) {
		submitfileGridDatatoServer($scope, $http);
	}
});

function submitfileGridDatatoServer($scope, $http)
{
	var griddata = [];
    var index=0
	angular.forEach($scope.map, function(value, key){
		console.log(key);
		if(value!=""){
		 griddata[index++]=value;
		}
	   });
    $scope.formData=griddata;
    if(index==0){
    	$("#submitresultspan").html("<br/>No data available to submit");
    	$("#submitresultspan").css({
    		"font-size" : "12px",
    		"color" : "black"
    	});
    	return true;
    }
    
    showWaitingBox("submitresultspan");
    var response = $http.post(url_addItemList, $scope.formData);
	response.success(function(data, status, headers, config) {
		if (data.statusCode== "SUCCESS") {
			var deviceDetailData = data.resultlist;
			var dataLength = deviceDetailData.length;
			if (dataLength > 0) {
				$("#submitresultspan").html("Record submitted successfully");
				$("#submitresultspan").css({
					"font-size" : "12px",
					"color" : "green"
				});
				setDataInScopeMapAddFindStatusCount($scope, deviceDetailData, false);
				showUploadedExcelSheetResult($scope, deviceDetailData,false);
				timeout("submitresultspan");
			}
			else{
				$("#submitresultspan").html("Record not submitted successfully");
				$("#submitresultspan").css({
					"font-size" : "12px",
					"color" : "red"
				});
				timeout("submitresultspan");
			}
		}
		else{
			$("#submitresultspan").html("Record not submitted successfully");
			$("#submitresultspan").css({
				"font-size" : "12px",
				"color" : "red"
			});
			manageServerReponse(data);
			timeout("submitresultspan");
		}
	});
	response.error(function(data, status, headers, config) {
		$("#searchresultspan").html(JSON.stringify({data: data}));
		$("#searchresultspan").css({
			"font-size" : "12px",
			"color" : "red"
		});
		manageFailedServerResponse(data);
	});
	

}
function uploadFiletoServer($scope,$q,$http){

	
	 var file = $scope.file;
	 if(typeof file=="undefined" || file==null)
		 {
		   $("#uploadresultspan").html("<br/>Please select valid file");
		   $("#uploadresultspan").css({"font-size" : "12px","color" : "red"});
		   return true;
		 }
	 
	 showWaitingBox("uploadresultspan")
	

     var deferred = $q.defer();
     var formData = new FormData();
         formData.append('file', file);
         formData.append('fileFormat', $scope.fileFormat);
         $http.post(url_upload, formData,{
             transformRequest : angular.identity,
             headers : {
                 'Content-Type' : undefined
             }})
             .then(
            	 function(serverresponse) {
         	    	var response=serverresponse.data;
         	    	if(response.statusCode=="SUCCESS"){
         	    			var resultData=response.resultlist;
         			    	var dataLength=resultData.length;
         					if (dataLength > 0) {
         						$("#uploadresultspan").html("");
         					} else {
         						$("#uploadresultspan").html("<br/>No data found");
         						$("#uploadresultspan").css({"font-size":"12px","color":"red"});
         					}
         					manageServerReponse(response);
         					setDataInScopeMapAddFindStatusCount($scope, resultData,true);
         					showUploadedExcelSheetResult($scope,resultData,true);
         	    	}
         	    	else
         	    	{
         	    		$("#uploadresultspan").html("");
         	    		manageServerReponse(response);
         	    	}
         	    	
         	    }, 
         	    function(response) { // optional
         	    	$("#uploadresultspan").html("<br/>Unable to complete request");
         	    	$("#uploadresultspan").css({"font-size":"12px","color":"red"});
         	    	manageFailedServerResponse(response); 
         	    }
             );
     deferred.promise;
     
     /*function (response) {
	 console.log(response);
	 $("#submitresultspan").html("");
     deferred.resolve(response.data);
	 },
	 function (errResponse) {
		 console.log(response);
	     alert(errResponse.data.errorMessage);
	     deferred.reject(errResponse);
	 }*/
}

function setDataInScopeMapAddFindStatusCount($scope,resultData,considerFlagtoReadData) 
{
	    $scope.showEntryPage=true;
		$scope.showResultPage=true;
		$scope.successcount=0;
		$scope.failedcount=0
		$scope.pendingcount=0
		$scope.map={};
		
		for(i=0;i<resultData.length;i++) {
			var resultlist=considerFlagtoReadData?resultData[i].resultlist:resultData[i];
			if(resultlist!=null){
				var resObj=considerFlagtoReadData?resultData[i].resultlist[0]:resultData[i];
				var status=resObj.status
				if(status=="" || status=="null" || status==null)
					status="Pending";
				
				if(status=="SUCCESS"){
					$scope.successcount=$scope.successcount+1;
				}else if(status=="FAILURE"){
					$scope.failedcount=$scope.failedcount+1;
				}else{
					$scope.pendingcount=$scope.pendingcount+1;
				}
				if(resObj.status!="FAILURE" && considerFlagtoReadData){
				$scope.map['map_'+i]=resObj;
				}
			}
		}
		
}

function showUploadedExcelSheetResult($scope,resultData,considerFlagtoReadData) {
		$.fn.dataTable.ext.errMode = 'none';
		$('#result_table').on('page.dt,error.dt', function() {
			console.log( 'An error has been reported by DataTables: ');
		}).DataTable({
			"data" : resultData,
			"fnDrawCallback" : function(oSettings) {
				/* applyColor(); */
			},
			"columns" : [{
				"data" :null,
				"render": function ( data, type, row ) {
					var val=considerFlagtoReadData?data.resultlist[0].productCode:data.productCode;
                    return val;
				}
			},{
				"data" :null,
				"render": function ( data, type, row ) {
					var val=considerFlagtoReadData?data.resultlist[0].product:data.product;
		            return val;
				}
			},{
				"data" :null,
				"render": function ( data, type, row ) {
					var val=considerFlagtoReadData?data.resultlist[0].action:data.action;
		            return val;
				}
			},{
				"data" :null,
				"render": function (data, type, row , meta) {
					var status=considerFlagtoReadData?data.resultlist[0].status:data.status;
					if(status=="" || status=="null" || status==null)
						status="Pending";
					
					var style="";
					if(status=="SUCCESS"){
						style="color:white;background-color:green";
					}
					else if(status=="FAILURE"){
						style="color:white;background-color:red";
					}
					else {
						style="";
					}
					if(considerFlagtoReadData)
						{
						  if(status=="Pending")
							  {
							  status="Validation not Required"
							  }
						  else{
							status="Validation "+status;
						  }
						}
				    return '<span style="'+style+'">'+status+'</span>';
				}
			},{
				"data" :null,
				"render": function ( data, type, row ,meta ) {
					var html='<img src="'+contextPath+'/system-activity/images/remove.png" id="'+meta.row+'" class="icon-delete" style="cursor: pointer;"></img>'
					var val=considerFlagtoReadData?html:'';
			        return val;
				}
			}],
			responsive : true,
			searching: true,
			"bSort" : true,
			"bDestroy" : true,
			"order": [],
			"initComplete" : function() {
				/* applyColor(); */
			}
		});

		var table = $('#result_table').DataTable();
		$('#result_table tbody').on( 'click', 'img.icon-delete', function () {
		    table.row( $(this).parents('tr') ).remove().draw();
			var id=$(this).attr("id");
			$scope.map['map_'+id]='';
		});
}

