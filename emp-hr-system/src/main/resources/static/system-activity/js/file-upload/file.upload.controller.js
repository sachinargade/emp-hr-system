app.controller('FileUploadController',function ($scope,FileUploadService,$http,$parse) {
	$("#headertitle").html("File Upload Activity");
	
	$scope.fileFormat="7";
	$scope.showEntryPage=false;
	$scope.showResultPage=false;
	$scope.showSubmitButton=true;
	
	$scope.successcount=0;
	$scope.failedcount=0;
	$scope.pendingcount=0;
	$scope.map={};
	
	$scope.uploadAgain=function(){
		$scope.showResultPage=false;
		var table = $('#result_table').DataTable();
		table.clear().draw();
		$('#uploadForm')[0].reset();
		$scope.file=null;
		$scope.map={};
    };
	
	$scope.uploadFile=function(){
		$scope.showSubmitButton=true;
		FileUploadService.uploadFile($scope);
    };
    
    $scope.submitfileGridData=function(){
    	$scope.showSubmitButton=false;
		FileUploadService.submitfileGridData($scope);
    };
});
	
app.directive('fileModel', [ '$parse', function($parse) {
    return {
        restrict : 'A',
        link : function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

