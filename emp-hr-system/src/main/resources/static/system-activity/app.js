'use strict';
/**
 * @ngdoc overview
 * @name SystemActivity
 * @description
 * # SystemActivity
 *
 * Main module of the application.
 */
var app=angular
  .module('SystemActivity', [
    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'nvd3',
    'cgPrompt'
  ]);

  app.config(['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider',function ($stateProvider,$urlRouterProvider,$ocLazyLoadProvider) {
    
    $ocLazyLoadProvider.config({
      debug:false,
      events:true,
    });

    $urlRouterProvider.otherwise('/system-activity/home');

    $stateProvider
      .state('system-activity', {
        url:'/system-activity',
        templateUrl: contextPath+'/system-activity/layout/landing-page.html',
        resolve: {
            loadMyDirectives:function($ocLazyLoad){
                return $ocLazyLoad.load(
                {
                    name:'SystemActivity',
                    files:[contextPath+'/system-activity/js/device.properties.js',
                           contextPath+'/system-activity/js/device.common.service.js',
                           contextPath+'/system-activity/js/file-upload/file.upload.controller.js',
                           contextPath+'/system-activity/js/file-upload/file.upload.service.js',
                           contextPath+'/system-activity/js/view-data/view.all.data.controller.js',
                           contextPath+'/system-activity/js/view-data/view.all.data.service.js',
                           contextPath+'/system-activity/js/add-data-by-grid/add.data.by.grid.controller.js',
                           contextPath+'/system-activity/js/add-data-by-grid/add.data.by.grid.service.js'
                          ]
                }),
                $ocLazyLoad.load(
                {
                   name:'toggle-switch',
                   files:[contextPath+'/system-activity/bower_components/angular-toggle-switch/angular-toggle-switch.min.js',
                          contextPath+'/system-activity/bower_components/angular-toggle-switch/angular-toggle-switch.css'
                      ]
                }),
                $ocLazyLoad.load(
                {
                  name:'ngAnimate',
                  files:[contextPath+'/system-activity/bower_components/angular-animate/angular-animate.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngCookies',
                  files:[contextPath+'/dashboar/system-activity/bower_components/angular-cookies/angular-cookies.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngResource',
                  files:[contextPath+'/system-activity/bower_components/angular-resource/angular-resource.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngSanitize',
                  files:[contextPath+'/system-activity/bower_components/angular-sanitize/angular-sanitize.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngTouch',
                  files:[contextPath+'/system-activity/bower_components/angular-touch/angular-touch.js']
                })
            }
        }
    })
      .state('system-activity.home',{
        url:'/home',
        controller: 'WelcomeCtrl',
        templateUrl:contextPath+'/system-activity/view-templates/welcome-page.html',
        resolve: {
            loadMyFile:function($ocLazyLoad) {
              $ocLazyLoad.load({
                  name:'SystemActivity',
                  files:[]
              });
            }
          }
      })
      .state('system-activity.fileUploadActivity',{
          url:'/fileUploadActivity',
          controller: 'FileUploadController',
          templateUrl:contextPath+'/system-activity/view-templates/file-upload-activity.html',
          resolve: {
              loadMyFile:function($ocLazyLoad) {
                $ocLazyLoad.load({
                    name:'SystemActivity',
                    files:[]
                });
              }
            }
        })
         .state('system-activity.addDataByGrid',{
          url:'/addDataByGrid',
          controller: 'AddDataByGridController',
          templateUrl:contextPath+'/system-activity/view-templates/add-data-by-grid.html',
          resolve: {
              loadMyFile:function($ocLazyLoad) {
                $ocLazyLoad.load({
                    name:'SystemActivity',
                    files:[]
                });
              }
            }
        })
         .state('system-activity.viewAllData',{
          url:'/viewAllData',
          controller: 'ViewAllDataController',
          templateUrl:contextPath+'/system-activity/view-templates/view-all-data.html',
          resolve: {
              loadMyFile:function($ocLazyLoad) {
                $ocLazyLoad.load({
                    name:'SystemActivity',
                    files:[]
                });
              }
            }
        })
          
  }]);
  