package com.emphrsystem.exceptions;

public class ApplicationServiceException extends Exception {

	/** Default Serial version ID. */
	private static final long serialVersionUID = 1L;

	public ApplicationServiceException(String errorMsg) {
		super(errorMsg);
	}

}
