/**
 * 
 */
package com.emphrsystem.exceptions;

/**
 * @author sargade
 *
 */
public class ApplicationDAOException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public ApplicationDAOException() {
	}

	/**
	 * @param message
	 */
	public ApplicationDAOException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ApplicationDAOException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ApplicationDAOException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ApplicationDAOException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
