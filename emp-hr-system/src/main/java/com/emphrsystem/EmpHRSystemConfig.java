package com.emphrsystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.emphrsystem.interceptor.EmpHRSystemInterceptor;

public class EmpHRSystemConfig extends WebMvcConfigurerAdapter {

	@Autowired
	EmpHRSystemInterceptor requestInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(requestInterceptor);
	}

}
