/**
 * 
 */
package com.emphrsystem.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Interceptor class
 * 
 * @author sargade
 *
 */
@Component
public class EmpHRSystemInterceptor extends HandlerInterceptorAdapter {

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String uri = request.getRequestURI();
		boolean flag = false;
		final boolean checkSessionIsValid = checkSessionIsValid(request);
		if (checkSessionIsValid || uri.contains("login") || uri.contains("authenticate") || uri.contains("error")) {
			flag = true;
		} else if (uri.contains("rest")) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"Unauthorized");
		} else {
			response.sendRedirect(request.getContextPath() + "/login");
		}
		return flag;
	}

	protected boolean checkSessionIsValid(HttpServletRequest request) throws Exception {

		final HttpSession userSession = request.getSession();
		if (userSession != null) {
			Object userId = userSession.getAttribute("userId");
			if (userId == null || "".equals(userId))
				return false;
		}
		return true;
	}

}
