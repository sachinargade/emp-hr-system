/**
 * 
 */
package com.emphrsystem.authentication.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.emphrsystem.authentication.model.UserVo;
import com.emphrsystem.authentication.service.IUserLoginProcess;

/**
 * @author sargade
 *
 */
@Controller
public class AuthenticationController {

	@Autowired
	private IUserLoginProcess userLoginProcess;

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login() {
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage() {
		return "login";
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index() {
		return "index";
	}

	/**
	 * @param model
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String viewLogin(HttpServletRequest request) throws IOException {

		HttpSession session = request.getSession(false);
		if (session != null) {
			LOGGER.info("Invalidating session and logging out.");
			session.invalidate();
		}
		LOGGER.info("validating request and logging in.");
		return "login";
	}

	/**
	 * @param model
	 * @param searsEnterpriseId
	 * @param password
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public String authenticateUserLogin(Model model,
			@RequestParam(value = "userName", required = true) String searsEnterpriseId,
			@RequestParam(value = "password", required = true) String password, HttpServletRequest request) throws IOException {

		try {
			 
			LOGGER.info("Validating User login process");
			final UserVo userVO = userLoginProcess.validateUser(searsEnterpriseId, password);
			LOGGER.info("creating a user session on successful validation");
			final HttpSession userSession = request.getSession();
			final String enterpriseId = userVO.getEnterpriseId();
			final String displayName = userVO.getDisplayName();
			final String role = userVO.getRole();
			userSession.setAttribute("userId", enterpriseId);
			userSession.setAttribute("requestorName", displayName);
			userSession.setAttribute("role", role);
			LOGGER.info("Session set for user: " + displayName + " user id: " + enterpriseId + " role: " + role);
			return "redirect:/index";
		} catch (Exception e) {
			LOGGER.error("Unsuccessful login attempt with Enterprise ID: " + searsEnterpriseId + " due to: "
					+ e.getMessage() + ". Redirecting user to login page");
			model.addAttribute("error", e.getMessage());
			return "login";
		}
	}

	/**
	 * @param model
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/secure/rest/getUserRole", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> getUserRole(ModelAndView model, HttpServletRequest request)
			throws IOException {

		HttpSession userSession = request.getSession();
		final String role = userSession.getAttribute("role").toString();
		final String displayName = userSession.getAttribute("requestorName").toString();
		Map<String, String> mp = new HashMap<String, String>();
		mp.put("role", role);
		mp.put("displayName", displayName);
		return mp;

	}

}
