package com.emphrsystem.authentication.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emphrsystem.authentication.dao.IUserLoginDAO;
import com.emphrsystem.authentication.model.UserVo;
import com.emphrsystem.exceptions.ApplicationDAOException;
import com.emphrsystem.exceptions.ApplicationServiceException;
import com.emphrsystem.utility.QueryConstants;

/**
 * @author pkumar2
 *
 */
@Service
public class UserLoginProcess implements IUserLoginProcess {

	@Autowired
	private IUserLoginDAO userRoleDao;
	private static final Logger LOGGER = Logger.getLogger(UserLoginProcess.class);

	/*
	 * (non-Javadoc)
	 * @see com.emphrsystem.authentication.service.impl.IUserRoleProcess#validateUser(java.lang.String, java.lang.String)
	 */
	@Override
	public UserVo validateUser(final String searsEnterpriseId, final String password)
			throws ApplicationServiceException {

		final String enterpriseId = searsEnterpriseId.trim();
		if (StringUtils.isBlank(enterpriseId) || StringUtils.isBlank(password)) {
			LOGGER.error("Please enter your enterprise ID and password");
			throw new ApplicationServiceException("Please enter your enterprise ID and password");
		}

		/* get user details from ldap */
		UserVo userVO = new UserVo();
		userVO.setDisplayName("Admin");
		userVO.setRole("L4");
		userVO.setEnterpriseId("1234");;
		
		try {
//			userVO = userRoleDao.validateUserCredentials(searsEnterpriseId,
//					QueryConstants.GET_USER_DETAILS);
			if (userVO != null && StringUtils.isBlank(userVO.getDisplayName())) {

				this.userRoleDao.updateDisplayName(enterpriseId, userVO.getDisplayName(),
						QueryConstants.UPDATE_USER);
				userVO.setDisplayName(userVO.getDisplayName());
			}

		} catch (ApplicationDAOException e) {
			throw new ApplicationServiceException("Application error encountered. Please try again.");
		}
		if (userVO == null || userVO.getEnterpriseId() == null) {
			throw new ApplicationServiceException(
					"The enterprise id provided is not authorised for accessing RES bypass services. "
							+ "Please contact Dev/L4 Team (Dev_L4_Team@emphrsystem.com) "
							+ "for access.");
		}

		LOGGER.info("User authenitcation successful for user: " + enterpriseId);
		return userVO;
	}
}