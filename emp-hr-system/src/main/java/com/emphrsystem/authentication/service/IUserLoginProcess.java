package com.emphrsystem.authentication.service;

import com.emphrsystem.authentication.model.UserVo;
import com.emphrsystem.exceptions.ApplicationServiceException;

public interface IUserLoginProcess {

	/**
	 * Validates and checks if the sears enterprise id is also a user or not
	 * 
	 * @param enterpriseId
	 *            The Sears enterprise user id
	 * @param password
	 *            The password
	 * @return returns list {@link UserVo}
	 * @throws ApplicationServiceException
	 *             {@link ApplicationServiceException}
	 */
	UserVo validateUser(final String enterpriseId,
			final String password) throws ApplicationServiceException;
}