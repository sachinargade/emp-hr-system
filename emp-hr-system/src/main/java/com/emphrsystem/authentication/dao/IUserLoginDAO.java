package com.emphrsystem.authentication.dao;

import com.emphrsystem.authentication.model.UserVo;
import com.emphrsystem.exceptions.ApplicationDAOException;

public interface IUserLoginDAO {

	/**
	 * Validates and gets user details for  user.
	 * 
	 * @param userName
	 *            The Sears enterprise user id
	 * @param query
	 *            The SQL query
	 * @return The Login details as {@link UserVo}.
	 * @throws ApplicationDAOException
	 *             {@link ApplicationDAOException}
	 */
	UserVo validateUserCredentials(final String userName,
			final String query) throws ApplicationDAOException;

	/**
	 * Update Display name for given enterprise user in  system.
	 * 
	 * @param enterpriseId
	 *            The Sears enterprise user id
	 * @param displayName
	 *            The display name of user
	 * @param query
	 *            The SQL query
	 * @throws ApplicationDAOException
	 *             {@link ApplicationDAOException}
	 */
	void updateDisplayName(final String enterpriseId, final String displayName,
			final String query) throws ApplicationDAOException;

}