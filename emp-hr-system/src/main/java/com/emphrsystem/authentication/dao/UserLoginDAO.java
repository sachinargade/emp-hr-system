/**
 * 
 */
package com.emphrsystem.authentication.dao;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.emphrsystem.authentication.model.UserVo;
import com.emphrsystem.exceptions.ApplicationDAOException;

/**
 * @author sargade
 *
 */
@Repository
public class UserLoginDAO implements
		IUserLoginDAO {

	private static final Logger LOGGER = Logger
			.getLogger(UserLoginDAO.class);

	
	/*
	 * (non-Javadoc)
	 * @see com.emphrsystem.authentication.dao.IUserRoleDAO#validateUserCredentials(java.lang.String, java.lang.String)
	 */
	@Override
	public UserVo validateUserCredentials(final String userName,
			final String query) throws ApplicationDAOException {
		return null;
	}

	/*
	 * 	@Override(non-Javadoc)
	 * @see com.emphrsystem.authentication.dao.IUserRoleDAO#updateDisplayName(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void updateDisplayName(final String enterpriseId,
			final String displayName, final String query)
			throws ApplicationDAOException {
	}
}